#Cliente
import socket
from time import time

host = socket.gethostname()
port = 5010
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
addr = (host, port)
send_size = 1024

f = open("./recursos/imagen.jpg", "rb")
#f = open("./recursos/archivo100MB.zip", "rb")
#f = open("./recursos/ISO.iso", "rb")

start_time = time()
while True:	
	bytes_read = f.readline(send_size)
	if not bytes_read:
		break
	s.sendto(bytes_read, addr)

elapsed_time = time() - start_time
print("Tiempo empleado: %0.7f seg." % elapsed_time)
f.close()
print ("Envio Finalizado")
s.close()
