#  Streaming Client

import socket
import time
import random

HOST = 'localhost'
PORT = 50008

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while True:
    data = s.recv(1024)
    time.sleep(random.uniform(0.05, 0.1))
    #time.sleep(0.5)
    print data

s.close()