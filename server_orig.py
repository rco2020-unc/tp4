# Streaming Server

import socket
import time
from random import randint
import random

HOST = 'localhost'
PORT = 50008

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

while True:
    conn, addr = s.accept()
    print 'Client connection accepted ', addr
    while True:
        try:
            #time.sleep(random.uniform(0.0001, 0.001))
            data = str(randint(0, 9)) + "\n"
            print 'Server sent:', data
            conn.send(data)    
        except socket.error, msg:
            print 'Client connection closed', addr
            break

conn.close()